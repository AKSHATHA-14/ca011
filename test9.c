#include<stdio.h>
void swap(int *p1,int *p2)
{
 int t;
 t=*p1;
 *p1=*p2;
 *p2=t;
 }
void main()
{
 int n1,n2;
 printf("Enter the first number\n");
 scanf("%d",&n1);
 printf("Enter the second number\n");
 scanf("%d",&n2);
 printf("The values of the numbers before swaping are %d ,%d\n",n1,n2);
 swap(&n1,&n2);
 printf("The values of the numbers after swaping are %d , %d",n1,n2);
 }
 